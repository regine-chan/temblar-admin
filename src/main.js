import Vue from 'vue';
import App from './App.vue'
import VueRouter from 'vue-router';
import TemblarUser from './views/TemblarUser.vue';
import Event from './views/Event.vue';
import EventType from './views/EventType.vue';
import Notification from "./views/Notification.vue";
import Subscription from "./views/Subscription.vue";
import Source from "./views/Source.vue";
import SourceSubscription from "./views/SourceSubscription.vue";
import ClientDevice from './views/ClientDevice.vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(VueRouter);

const routes = [
  {
    path: "/temblar-user",
    name: "temblar-user",
    component: TemblarUser
  },
  {
    path: "/event",
    name: "event",
    component: Event
  },
  {
    path: "/event-type",
    name: "event-type",
    component: EventType
  },
  {
    path: "/notification",
    name: "notification",
    component: Notification
  },
  {
    path: "/subscription",
    name: "subscription",
    component: Subscription
  },
  {
    path: "/source",
    name: "source",
    component: Source
  },
  {
    path: "/client-device",
    name: "client-device",
    component: ClientDevice
  },
  {
    path: "/source-subscription",
    name: "source-subscription",
    component: SourceSubscription
  },
  {
    path: "/",
    redirect: "/temblar-user"
  }
];

const router = new VueRouter({
  routes
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router
}).$mount('#app');
