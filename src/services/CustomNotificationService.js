import { BASE_URL } from '../environment';

export const clearCustomNotifications = () => {
    const options = {
        method: "DELETE"
    };
    return fetch(BASE_URL + "/CustomNotifications/clear", options).then(res => {
        if (res.ok) {
            return true;
        } else {
            return false;
        }
    });
}

// Fetches all temblar users
export const getCustomNotifications = () => {
    return fetch(BASE_URL + "/CustomNotifications").then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch customNotifications";
        }
    }).then(res => {
        return res.map(value => Object.assign({}, { customNotification: value }, { isEditActive: false }));
    });
}

// decides if an entity is to be created or updated
export const saveCustomNotification = customNotification => {
    if (customNotification.id) {
        return updateCustomNotification(customNotification);
    } else {
        return createCustomNotification(customNotification);
    }
}

// Creates a temblar user
const createCustomNotification = customNotification => {
    if (customNotification.eventId !== "") {
        const body = JSON.stringify(customNotification);
        const options = {
            method: "POST",
            body: body,
            headers: { "Content-Type": "application/json" }
        };
        return fetch(BASE_URL + "/CustomNotifications", options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Deletes
export const deleteCustomNotification = customNotificationId => {
    if (customNotificationId !== "") {
        const options = {
            method: "DELETE"
        };
        return fetch(BASE_URL + "/CustomNotifications/" + customNotificationId, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Updates
const updateCustomNotification = customNotification => {
    if (customNotification.id !== "") {
        const options = {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(customNotification)
        };
        return fetch(BASE_URL + "/CustomNotifications/" + customNotification.id, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        })
    } else return false;
}