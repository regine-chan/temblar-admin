import { BASE_URL } from '../environment';

export const clearClientDevices = () => {
        const options = {
            method: "DELETE"
        };
        return fetch(BASE_URL + "/ClientDevice/clear", options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
}

// Fetches all temblar users
export const getClientDevices = () => {
    return fetch(BASE_URL + "/ClientDevice").then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch clientDevices";
        }
    }).then(res => {
        return res.map(value => Object.assign({}, { clientDevice: value }, { isEditActive: false }));
    });
}

// decides if an entity is to be created or updated
export const saveClientDevice = clientDevice => {
    if (clientDevice.id) {
        return updateClientDevice(clientDevice);
    } else {
        return createClientDevice(clientDevice);
    }
}

// Creates a temblar user
const createClientDevice = clientDevice => {
    if (clientDevice.eventId !== "") {
        const body = JSON.stringify(clientDevice);
        const options = {
            method: "POST",
            body: body,
            headers: { "Content-Type": "application/json" }
        };
        return fetch(BASE_URL + "/ClientDevice", options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Deletes
export const deleteClientDevice = clientDeviceId => {
    if (clientDeviceId !== "") {
        const options = {
            method: "DELETE"
        };
        return fetch(BASE_URL + "/ClientDevice/" + clientDeviceId, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Updates
const updateClientDevice = clientDevice => {
    if (clientDevice.id !== "") {
        const options = {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(clientDevice)
        };
        return fetch(BASE_URL + "/ClientDevice/" + clientDevice.id, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        })
    } else return false;
}