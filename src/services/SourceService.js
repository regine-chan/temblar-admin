import { BASE_URL } from '../environment';

export const clearSources = () => {
    const options = {
        method: "DELETE"
    };
    return fetch(BASE_URL + "/Sources/clear", options).then(res => {
        if (res.ok) {
            return true;
        } else {
            return false;
        }
    });
}

// Fetches all temblar users
export const getSources = () => {
    return fetch(BASE_URL + "/Sources/admin").then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch sources";
        }
    }).then(res => {
        return res.map(value => Object.assign({}, { source: value }, { isEditActive: false }));
    });
}

// decides if an entity is to be created or updated
export const saveSource = source => {
    if (source.id) {
        return updateSource(source);
    } else {
        return createSource(source);
    }
}

// Creates a temblar user
const createSource = source => {
    if (source.eventId !== "") {
        const body = JSON.stringify(source);
        const options = {
            method: "POST",
            body: body,
            headers: { "Content-Type": "application/json" }
        };
        return fetch(BASE_URL + "/Sources", options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Deletes
export const deleteSource = sourceId => {
    if (sourceId !== "") {
        const options = {
            method: "DELETE"
        };
        return fetch(BASE_URL + "/Sources/admin/" + sourceId, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Updates
const updateSource = source => {
    if (source.id !== "") {
        const options = {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(source)
        };
        return fetch(BASE_URL + "/Sources/" + source.id, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        })
    } else return false;
}