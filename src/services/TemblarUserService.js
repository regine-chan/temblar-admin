import { BASE_URL } from '../environment';

export const clearTemblarUsers = () => {
    const options = {
        method: "DELETE"
    };
    return fetch(BASE_URL + "/TemblarUsers/clear", options).then(res => {
        if (res.ok) {
            return true;
        } else {
            return false;
        }
    });
}

// Fetches all temblar users
export const getTemblarUsers = () => {
    return fetch(BASE_URL + "/TemblarUsers").then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch events";
        }
    }).then(res => {
        return res.map(value => Object.assign({}, { user: value }, { isEditActive: false }));
    });
}

// decides if an entity is to be created or updated
export const saveTemblarUser = user => {
    if (user.id) {
        return updateTemblarUser(user);
    } else {
        return createTemblarUser(user);
    }
}

// Creates a temblar user
const createTemblarUser = user => {
    if (user.username !== "") {
        const body = JSON.stringify(user);
        const options = {
            method: "POST",
            body: body,
            headers: { "Content-Type": "application/json" }
        };
        return fetch(BASE_URL + "/TemblarUsers", options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Deletes
export const deleteTemblarUser = userId => {
    if (userId !== "") {
        const options = {
            method: "DELETE"
        };
        return fetch(BASE_URL + "/TemblarUsers/" + userId, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Updates
const updateTemblarUser = user => {
    if (user.id !== "") {
        const options = {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(user)
        };
        return fetch(BASE_URL + "/TemblarUsers/" + user.id, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        })
    } else return false;
}