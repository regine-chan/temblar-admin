import { BASE_URL } from '../environment';

export const clearEventTypes = () => {
    const options = {
        method: "DELETE"
    };
    return fetch(BASE_URL + "/EventTypes/clear", options).then(res => {
        if (res.ok) {
            return true;
        } else {
            return false;
        }
    });
}

// Fetches all temblar users
export const getEventTypes = () => {
    return fetch(BASE_URL + "/EventTypes").then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch eventTypes";
        }
    }).then(res => {
        return res.map(value => Object.assign({}, { eventType: value }, { isEditActive: false }));
    });
}

export const getEventTypesForSelect = () => {
    return fetch(BASE_URL + "/EventTypes").then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch eventTypes";
        }
    }).then(res => {
        return res;
    });
}

// decides if an entity is to be created or updated
export const saveEventType = eventType => {
    if (eventType.id) {
        return updateEventType(eventType);
    } else {
        return createEventType(eventType);
    }
}

// Creates a temblar user
const createEventType = eventType => {
    if (eventType.eventId !== "") {
        const body = JSON.stringify(eventType);
        const options = {
            method: "POST",
            body: body,
            headers: { "Content-Type": "application/json" }
        };
        return fetch(BASE_URL + "/EventTypes", options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Deletes
export const deleteEventType = eventTypeId => {
    if (eventTypeId !== "") {
        const options = {
            method: "DELETE"
        };
        return fetch(BASE_URL + "/EventTypes/" + eventTypeId, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Updates
const updateEventType = eventType => {
    if (eventType.id !== "") {
        const options = {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(eventType)
        };
        return fetch(BASE_URL + "/EventTypes/" + eventType.id, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        })
    } else return false;
}