import { BASE_URL } from '../environment';

export const clearSubscriptions = () => {
    const options = {
        method: "DELETE"
    };
    return fetch(BASE_URL + "/Subscriptions/clear", options).then(res => {
        if (res.ok) {
            return true;
        } else {
            return false;
        }
    });
}

// Fetches all temblar users
export const getSubscriptions = () => {
    return fetch(BASE_URL + "/Subscriptions").then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch subscriptions";
        }
    }).then(res => {
        return res.map(value => Object.assign({}, { subscription: value }, { isEditActive: false }));
    });
}

// decides if an entity is to be created or updated
export const saveSubscription = subscription => {
    return createSubscription(subscription);
}

// Creates a temblar user
const createSubscription = subscription => {
    if (subscription.eventId !== "") {
        const body = JSON.stringify(subscription);
        const options = {
            method: "POST",
            body: body,
            headers: { "Content-Type": "application/json" }
        };
        return fetch(BASE_URL + "/Subscriptions", options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Deletes
export const deleteSubscription = (eventTypeId, temblarUserId) => {
    if (eventTypeId !== "" && temblarUserId !== "") {
        const options = {
            method: "DELETE"
        };
        return fetch(`${BASE_URL}/Subscriptions/${eventTypeId}&${temblarUserId}`, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Unused
/*
const updateSubscription = subscription => {
    if (subscription.id !== "") {
        const options = {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(subscription)
        };
        return fetch(BASE_URL + "/Subscriptions/" + subscription.id, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        })
    } else return false;
}*/