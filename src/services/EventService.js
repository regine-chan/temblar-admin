import { BASE_URL } from '../environment';

export const clearEvents = () => {
    const options = {
        method: "DELETE"
    };
    return fetch(BASE_URL + "/Events/clear", options).then(res => {
        if (res.ok) {
            return true;
        } else {
            return false;
        }
    });
}

// Fetches all temblar users
export const getEvents = () => {
    return fetch(BASE_URL + "/Events").then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch event";
        }
    }).then(res => {
        return res.map(value => Object.assign({}, { event: value }, { isEditActive: false }));
    });
}

// decides if an entity is to be created or updated
export const saveEvent = event => {
    if (event.id) {
        return updateEvent(event);
    } else {
        return createEvent(event);
    }
}

// Creates a temblar user
const createEvent = event => {
    if (event.eventId !== "") {
        const body = JSON.stringify(event);
        const options = {
            method: "POST",
            body: body,
            headers: { "Content-Type": "application/json" }
        };
        return fetch(BASE_URL + "/Events", options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Deletes
export const deleteEvent = eventId => {
    if (eventId !== "") {
        const options = {
            method: "DELETE"
        };
        return fetch(BASE_URL + "/Events/" + eventId, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Updates
const updateEvent = event => {
    if (event.id !== "") {
        const options = {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(event)
        };
        return fetch(BASE_URL + "/Events/" + event.id, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        })
    } else return false;
}