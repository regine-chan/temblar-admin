import { BASE_URL } from '../environment';

export const clearSourceSubscriptions = () => {
    const options = {
        method: "DELETE"
    };
    return fetch(BASE_URL + "/SourceSubscriptions/clear", options).then(res => {
        if (res.ok) {
            return true;
        } else {
            return false;
        }
    });
}


// Fetches all temblar users
export const getSourceSubscriptions = () => {
    return fetch(BASE_URL + "/SourceSubscriptions").then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch sourceSubscriptions";
        }
    }).then(res => {
        return res.map(value => Object.assign({}, { sourceSubscription: value }, { isEditActive: false }));
    });
}

// decides if an entity is to be created or updated
export const saveSourceSubscription = sourceSubscription => {

    return createSourceSubscription(sourceSubscription);

}

// Creates a temblar user
const createSourceSubscription = sourceSubscription => {
    if (sourceSubscription.eventId !== "") {
        const body = JSON.stringify(sourceSubscription);
        const options = {
            method: "POST",
            body: body,
            headers: { "Content-Type": "application/json" }
        };
        return fetch(BASE_URL + "/SourceSubscriptions", options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Deletes
export const deleteSourceSubscription = (sourceId, temblarUserId) => {
    if (sourceId !== "" && temblarUserId !== "") {
        const options = {
            method: "DELETE"
        };
        return fetch(`${BASE_URL}/SourceSubscriptions/${sourceId}&${temblarUserId}`, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        });
    } else return false;
}

// Unused
/*
const updateSourceSubscription = sourceSubscription => {
    if (sourceSubscription.id !== "") {
        const options = {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(sourceSubscription)
        };
        return fetch(BASE_URL + "/SourceSubscriptions/" + sourceSubscription.id, options).then(res => {
            if (res.ok) {
                return true;
            } else {
                return false;
            }
        })
    } else return false;
}
*/