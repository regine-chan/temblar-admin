const PROD_BASE_URL = "https://temblar-dev.azurewebsites.net/api";
const DEV_BASE_URL = "https://localhost:44348/api";
const DEV_ENV = false;
export const BASE_URL = DEV_ENV ? DEV_BASE_URL : PROD_BASE_URL;